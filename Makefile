ifndef DOWNLOAD_DATADIR
	override DOWNLOAD_DATADIR=download_data
endif

ifndef POSTGIS_PERF_DB
	override POSTGIS_PERF_DB=postgis_perf
endif

TESTSITE = "http://postgis.net/extra/test-data"

POSTGIS_VERSION ?= $(shell \
	psql -tAXc "select default_version \
	FROM pg_available_extensions \
	WHERE name = 'postgis'" \
)

PSQL_OPTS = -d $(POSTGIS_PERF_DB) --set ON_ERROR_STOP=1

.PHONY: help
help: ## Show this help screen
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: download-data
download-data: ## Downloads all data files from postgis.net. Puts in folder defined by env $DOWNLOAD_DATADIR
	sh download_data.sh

$(DOWNLOAD_DATADIR)/%.sql.bz2: ##Downloads data selectively
	mkdir -p $(DOWNLOAD_DATADIR)
	cd $(DOWNLOAD_DATADIR)
	wget -nc $(TESTSITE)/$(notdir $@) -O $@

$(DOWNLOAD_DATADIR)/%.sql: $(DOWNLOAD_DATADIR)/%.sql.bz2
	cd $(DOWNLOAD_DATADIR)
	bzip2 -dk $<

%.sql: %.sql.in Makefile ## Builds sql files as needed
	perl -lpe "s'@DOWNLOAD_DATADIR@'$(DOWNLOAD_DATADIR)'g;s'@POSTGIS_VERSION@'$(POSTGIS_VERSION)'g;s'@FAST_GIST_BUILD@'$(FAST_GIST_BUILD)'g;s'@POSTGIS_PERF_DB@'$(POSTGIS_PERF_DB)'g" $< > $@

clean: ## Delete generated files
	rm -f setupdb_*.sql *.out

prep-postgis: setupdb_postgis.sql $(DOWNLOAD_DATADIR)/osm_belarus.sql $(DOWNLOAD_DATADIR)/osm_china.sql $(DOWNLOAD_DATADIR)/roads_rdr.sql ## Create database and load with data
	psql -c "DROP DATABASE IF EXISTS $(POSTGIS_PERF_DB);"
	psql -c "CREATE DATABASE $(POSTGIS_PERF_DB);"
	psql $(PSQL_OPTS) -f setupdb_postgis.sql -o load.out

.PHONY: Basic postgis performance tests
test-postgis: ## Run postgis tests
	pg_prove --failures --timer --comments --verbose --recurse --ext .sql -d "$(POSTGIS_PERF_DB)" pgtap/postgis

.PHONY: PostGIS Index performance tests
test-postgis-index-performance: ## Run postgis tests
	pg_prove --failures --timer --comments --verbose --recurse --ext .sql -d "$(POSTGIS_PERF_DB)" pgtap/postgis_index_performance

prep-tiger-geocoder: setupdb_tiger_geocoder.sql $(DOWNLOAD_DATADIR)/tiger_national.sql $(DOWNLOAD_DATADIR)/tiger_dc.sql $(DOWNLOAD_DATADIR)/tiger_ma.sql ## Create tiger database and load with data
	psql -c "DROP DATABASE IF EXISTS $(POSTGIS_PERF_DB);"
	psql -c "CREATE DATABASE $(POSTGIS_PERF_DB);"
	psql $(PSQL_OPTS) -f setupdb_tiger_geocoder.sql -o load_tiger.out

.PHONY: Tiger geocoder performance tests
test-tiger-geocoder: ## Run tiger geocoder tests
	pg_prove --failures --timer --verbose --recurse --ext .sql -d "$(POSTGIS_PERF_DB)" pgtap/tiger_geocoder

prep-topology: setupdb_topology.sql $(DOWNLOAD_DATADIR)/tiger_national.sql $(DOWNLOAD_DATADIR)/tiger_dc.sql ## Create topology database and load with data
	psql -c "DROP DATABASE IF EXISTS $(POSTGIS_PERF_DB);"
	psql -c "CREATE DATABASE $(POSTGIS_PERF_DB);"
	psql $(PSQL_OPTS) -f setupdb_topology.sql -o load_topology.out

test-topology: ## Run topology tests
	pg_prove --failures --timer  --verbose --recurse --ext .sql -d "$(POSTGIS_PERF_DB)" pgtap/topology

prep-raster: setupdb_raster.sql $(DOWNLOAD_DATADIR)/osm_belarus.sql ## Create database for raster tests and load with data
	psql -c "DROP DATABASE IF EXISTS $(POSTGIS_PERF_DB);"
	psql -c "CREATE DATABASE $(POSTGIS_PERF_DB);"
	psql $(PSQL_OPTS) -f setupdb_raster.sql -o load_raster.out

test-raster: ## Run raster tests
	pg_prove --failures --timer --verbose --recurse --ext .sql -d "$(POSTGIS_PERF_DB)" pgtap/raster

prep-sfcgal: setupdb_sfcgal.sql $(DOWNLOAD_DATADIR)/countries.sql ## Create database for sfcgal tests and load with data
	psql -c "DROP DATABASE IF EXISTS $(POSTGIS_PERF_DB);"
	psql -c "CREATE DATABASE $(POSTGIS_PERF_DB);"
	psql $(PSQL_OPTS) -f setupdb_sfcgal.sql -o load_sfcgal.out

test-sfcgal: ## Run sfcgal tests
	pg_prove --failures --timer --comments --verbose --recurse --ext .sql -d "$(POSTGIS_PERF_DB)" pgtap/sfcgal
