export PGPORT=5451
export DOWNLOAD_DATADIR=/tmp #on windows C:/temp
export POSTGIS_VERSION=3.2.0dev
export PGPATH=/projects/postgresql/rel/pg14w64gcc81
export PGUSER=postgres
export PATH=${PATH}:${PGPATH}/bin
export POSGIS_PERF_DB=postgis_perf
make clean
#download-data is no longer needed for prep because each prep defines target data to dowload
#make download-data
make prep-postgis
make test-postgis
make test-postgis-index-performance
make prep-topology
make test-topology
make prep-raster
make test-raster
make prep-sfcgal
make test-sfcgal
make prep-tiger-geocoder
make test-tiger-geocoder
