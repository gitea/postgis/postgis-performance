#!/usr/bin/env bash
set -e
if  [[ "${OVERRIDE}" == '' ]] ; then
	export GEOS_VER=3.11
	export GDAL_VER=3.3.3
	export PROJ_VER=7.2.1
	export SFCGAL_VER=1.3.9
	export CGAL_VER=5.0.2
	export ICON_VER=1.15
	export ZLIB_VER=1.2.11
  export PROTOBUF_VER=3.2.0
	export PROTOBUFC_VER=1.2.1
	export JSON_VER=0.12
	export PROJSO=libproj-19.dll
fi;

#export OS_BUILD=64
#export PG_VER=14
#export GCC
export PGPATH=/projects/postgresql/rel/pg${PG_VER}w${OS_BUILD}gcc81
export PGUSER=postgres
export PATH=${PATH}:${PGPATH}/bin

export PROTOBUF_VER=3.2.0
export PROTOBUFC_VER=1.2.1
export JSON_VER=0.12
export PCRE_VER=8.33

if  [[ "${ICON_VER}" == '' ]] ; then
  export ICON_VER=1.16
fi;

echo "ICON_VER ${ICON_VER}"

#set to something even if override is on but not set
if  [[ "${ZLIB_VER}" == '' ]] ; then
  export ZLIB_VER=1.2.11
fi;


#set to something even if override is on but not set
if  [[ "${LIBXML_VER}" == '' ]] ; then
  export LIBXML_VER=2.9.9
fi;

#set to something even if override is on but not set
if  [[ "${CGAL_VER}" == '' ]] ; then
  export CGAL_VER=5.0
fi;

echo "ZLIB_VER $ZLIB_VER"
echo "PROJ_VER $PROJ_VER"
echo "LIBXML_VER $LIBXML_VER"
echo "CGAL_VER $CGAL_VER"
echo "ZLIB_VER $ZLIB_VER"
echo "PROJ_VER $PROJ_VER"
echo "LIBXML_VER $LIBXML_VER"


if [ "$OS_BUILD" == "64" ] ; then
	export MINGHOST=x86_64-w64-mingw32
else
	export MINGHOST=i686-w64-mingw32
fi;

export DOWNLOAD_DATADIR=C:/Temp
export POSTGIS_VERSION=3.3.1
cd ${WORKSPACE}
for POSTGIS_VERSION in 3.3.1 master; do
  make clean
  if  test "$POSTGIS_VERSION" = "3.3.1"; then
    export POSTGIS_PERF_DB="postgis_perf_3_3"
  fi

  make prep-postgis
  make test-postgis-index-performance
  # make prep-tiger-geocoder
  # make test-tiger-geocoder
  # make prep-topology
  # make test-topology
  # make prep-raster
  # make test-raster
  # make prep-sfcgal
  # make test-sfcgal
done
