#!/bin/bash
# Reallie is a 64-bit Debian 11, residing in same host as debbie
# This script builds a passed in version of PostGIS for testing

export WORKSPACE=/home/jenkins/workspace
export DOWNLOAD_DATADIR=${WORKSPACE}/download_data
export POSTGIS_REPO=${WORKSPACE}/postgis-git
export BRANCH=master
export OS_BUILD=64
export PG_VER=15
export PGPATH=${WORKSPACE}/pg/label/${label}/rel/pg${PG_VER}w${OS_BUILD}

export PATH=${PGPATH}/bin:${PGPATH}/lib:${PATH}
export PGPORT=55432
export PGDATA=$PGPATH/data_${PGPORT}
export PGHOST=localhost
export PGEXTENSION=${PGPATH}/share/postgresql/extension


if [ ! -d "$POSTGIS_REPO" ]; then
  git clone https://git.osgeo.org/gitea/postgis/postgis.git ${POSTGIS_REPO}
fi

## build in order so we can use for upgrade tests later
for BRANCH in master 3.3.1; do
if [ ! -f "$PGEXTENSION/postgis--${BRANCH}.sql" ]; then
		cd ${POSTGIS_REPO}
		git clean -fd
		git reset --hard
		git pull
		git checkout $BRANCH
		git pull
		echo "Building $BRANCH"
		sh autogen.sh
		#the --prefix is to prevent liblwgeom from being installed in system which jenkins is not allowed to do
		./configure --with-pgconfig=${PGPATH}/bin/pg_config --with-library-minor-version  --prefix=${PGPATH}/bin
		make clean
		make
		export err_status=0
		make install
		err_status=$?
fi
done
