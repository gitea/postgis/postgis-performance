export WORKSPACE=/home/jenkins/workspace
export DOWNLOAD_DATADIR=${WORKSPACE}/download_data
export POSTGIS_VERSION=3.3.0dev
export OS_BUILD=64
export PG_VER=15
export PGPATH=${WORKSPACE}/pg/label/${label}/rel/pg${PG_VER}w${OS_BUILD}
export PATH=${PATH}:${PGPATH}/bin:${PGPATH}/lib
export PGPORT=55432
export PGDATA=$PGPATH/data_${PGPORT}
export PGLOG="$PGDATA/pgsql.log"
export PGDATABASE=postgres

for POSTGIS_VERSION in 3.3.1 3.4.0dev; do
  make clean
  if  test "$POSTGIS_VERSION" = "3.3.1"; then
    export POSTGIS_PERF_DB="postgis_perf_3_3"
  fi

  make prep-postgis
  make test-postgis
  make test-postgis-index-performance
  make prep-tiger-geocoder
  make test-tiger-geocoder
  make prep-topology
  make test-topology
  make prep-raster
  make test-raster
  make prep-sfcgal
  make test-sfcgal
done


if [ -d $PGDATA/postmaster.pid ] ; then
  $PGCTL stop -D $PGDATA -s -m fast
fi
exit $err_status
