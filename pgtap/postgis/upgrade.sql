\i pgtap/setup.sql
SELECT plan(2);
SELECT 'Before upgrade: ' || postgis_full_version();

-- test only if our version is lower or same as the default
SELECT CASE WHEN NOT (e.installed_version <= e.default_version)
    THEN skip('Can not upgrade from ' || e.installed_version || ' to  ' || e.default_version, 2 )
    ELSE
    collect_tap(
        performs_ok('SELECT postgis_extensions_upgrade()', 5000, 'upgrade'),
        performs_ok('SELECT postgis_extensions_upgrade()', 5000, 'upgrade')
    ) END
FROM pg_available_extensions AS e
WHERE name = 'postgis';

SELECT 'After  upgrade: ' || postgis_full_version();
SELECT * FROM finish();
