\i pgtap/setup.sql
SELECT plan(1);
PREPARE poly_transform_mercator AS SELECT sum(ST_Area(ST_Transform(geom,3857))) FROM osm_belarus.multipolygons WHERE ST_Intersects(geom, ST_MakeEnvelope(27.5, 53.9, 27.6, 55, 4326) ) ;

-- 20 iterations, should not take longer than avg 2500 per iteration
SELECT performs_within('poly_transform_mercator', 5, 2500, 20, 'poly_transform_mercator');

SELECT * FROM finish();
