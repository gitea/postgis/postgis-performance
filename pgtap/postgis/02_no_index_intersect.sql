\i pgtap/setup.sql
SELECT plan(4);
DROP INDEX IF EXISTS osm_belarus.ix_osm_belarus_multipolygons_gist;
DROP INDEX IF EXISTS osm_belarus.ix_osm_belarus_lines_gist;
analyze;

PREPARE intersect_check_multipolygon_no_match AS SELECT * FROM osm_belarus.multipolygons WHERE ST_Intersects(geom, ST_SetSRID(ST_Point(10,10),4326) );

PREPARE intersect_check_multipolygon_match AS SELECT * FROM osm_belarus.multipolygons WHERE ST_Intersects(geom, ST_MakeEnvelope(27.5, 53.9, 27.6, 55, 4326) );

PREPARE intersect_check_line_no_match AS SELECT * FROM osm_belarus.lines WHERE ST_Intersects(geom, ST_SetSRID(ST_Point(10,10),4326) );

PREPARE intersect_check_line_match AS SELECT * FROM osm_belarus.lines WHERE ST_Intersects(geom, ST_MakeEnvelope(27.5, 53.9, 27.6, 55, 4326) );

-- check before index
-- run 50 iterations and require each to finish between 0 and 3200 ms
SELECT performs_within('intersect_check_multipolygon_no_match', 0, 3200, 50, 'intersect multipolygon check no match no index');

SELECT performs_within('intersect_check_multipolygon_match', 0, 3200, 50, 'intersect multipolygon check match no index');

SELECT performs_within('intersect_check_line_no_match', 0, 1200, 50, 'intersect linestring check no match no index');

SELECT performs_within('intersect_check_line_match', 0, 1200, 50, 'intersect linestring check match no index');

SELECT * FROM finish();






