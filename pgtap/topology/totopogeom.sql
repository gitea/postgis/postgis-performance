\i pgtap/setup.sql
SELECT plan(6);
SELECT topology.CreateTopology('topo_dc', 4269);
-- create a new table
CREATE TABLE public.dc_zcta5_topo(gid serial primary key, zip varchar(5));
--add a topogeometry column to it
SELECT topology.AddTopoGeometryColumn('topo_dc', 'public', 'dc_zcta5_topo', 'topo', 'MULTIPOLYGON') As new_layer_id;

PREPARE add_topos AS INSERT INTO public.dc_zcta5_topo(zip, topo)
SELECT zcta5ce,  topology.toTopoGeom(the_geom, 'topo_dc', 1)
FROM tiger.zcta5
WHERE statefp = '11';

-- takes 1072.966 ms on my computer
SELECT performs_ok( 'add_topos', 2000, 'Add DC zips') ;

SELECT pass((SELECT count(1) FROM topo_dc.face)::text || ' faces');

SELECT pass((SELECT count(1) FROM topo_dc.edge)::text || ' edges');

SELECT 'Area of TOPO 20001 = ' || (SELECT ST_Area(topo::geometry::geography)::integer FROM public.dc_zcta5_topo WHERE zip = '20001')::text;

SELECT 'Area of original 20001 = ' || (SELECT ST_Area(the_geom::geometry::geography)::integer FROM tiger.zcta5 WHERE zcta5ce = '20001')::text;

SELECT 'Diff of areas ' || abs( (SELECT ST_Area(topo::geometry::geography)::integer FROM public.dc_zcta5_topo WHERE zip = '20001') - (SELECT ST_Area(the_geom::geometry::geography)::integer FROM tiger.zcta5 WHERE zcta5ce = '20001') )::text;

SELECT ok(abs( (SELECT ST_Area(topo::geometry::geography)::integer FROM public.dc_zcta5_topo WHERE zip = '20001') - (SELECT ST_Area(the_geom::geometry::geography)::integer FROM tiger.zcta5 WHERE zcta5ce = '20001') ) < 145, 'Areas of orig and topo 20001 within 145');

-- test new feature introduced in PostGIS 3.2
SELECT CASE WHEN postgis_lib_version() < '3.2'
    THEN skip('TopoGeom_addTopoGeometry not supported before 3.2', 2 )
    ELSE
    collect_tap(
        performs_ok( $sql$UPDATE public.dc_zcta5_topo SET topo = TopoGeom_addTopoGeom(
        topo,
        (SELECT f.topo FROM public.dc_zcta5_topo AS f WHERE zip = '20002' )
    )
      WHERE zip = '20001'
      RETURNING *;$sql$, 100000, 'TopoGeom_addTopoGeometry: Add zip 20002 to 20001'),
      pass('New area: ' || (SELECT ST_Area(topo::geometry::geography)::integer FROM public.dc_zcta5_topo WHERE zip = '20001')::text)
    ) END;

SELECT * FROM finish();
ROLLBACK;
