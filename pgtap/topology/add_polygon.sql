\i pgtap/setup.sql
SELECT plan(3);
SELECT topology.CreateTopology('topoperf', 4269) ;

CREATE TABLE topoperf.polygon
AS SELECT (ST_Dump(geom)).geom AS g FROM (SELECT the_geom AS geom FROM tiger.state) AS f;

PREPARE add_polygon AS SELECT count(*) FROM (
  SELECT topology.TopoGeo_addPolygon('topoperf', g)
  FROM topoperf.polygon
) foo;
-- should complete in less than 100 seconds, took 53729.350 ms on my computer
SELECT performs_ok( 'add_polygon', 100000, 'US States polygons added') ;

SELECT pass((SELECT count(1) FROM topoperf.face)::text || ' faces');

SELECT pass((SELECT count(1) FROM topoperf.edge)::text || ' edges');
SELECT * FROM finish();
ROLLBACK;