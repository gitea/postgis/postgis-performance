\i pgtap/setup.sql
SELECT plan(6);
SELECT topology.CreateTopology('topo_dc', 4269) ;
SELECT has_schema( 'topo_dc', 'topo_dc created' );
-- add dc - should take under 300 ms
PREPARE add_dc AS SELECT ST_CreateTopoGeo('topo_dc', ST_Collect(the_geom))
FROM tiger.place WHERE statefp = '11';
SELECT performs_ok( 'add_dc', 300, 'Add dc to topology') ;
SELECT topology.CreateTopology('topo_states', 4269);
SELECT has_schema( 'topo_states', 'Created states topology');

-- add states - should take under 20 seconds (on my computer takes 6 secs but reallie is really slow)
PREPARE add_states AS SELECT ST_CreateTopoGeo('topo_states', ST_Collect(the_geom))
FROM tiger.state;
SELECT performs_ok( 'add_states', 20000, 'Add states') ;

-- create topology for counties
SELECT topology.CreateTopology('topo_counties', 4269);
SELECT has_schema( 'topo_counties', 'Created counties topology');

-- TODO: strk look at this one, not sure it's reallie (as it works fine on my pc with full 3234 counties)
-- add MA counties - should take under 50 seconds (reduced to just MA cause crashes on reallie otherwise) (only 14 counties in MA)
PREPARE add_counties AS SELECT ST_CreateTopoGeo('topo_counties', ST_Collect(the_geom))
FROM tiger.county WHERE statefp = '25';
SELECT performs_ok( 'add_counties', 50000, 'Add counties') ;

SELECT * FROM finish();
ROLLBACK;