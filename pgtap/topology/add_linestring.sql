\i pgtap/setup.sql
SELECT plan(3);
SELECT topology.CreateTopology('topoperf', 4269) ;

CREATE TABLE topoperf.case_full_coverage_no_holes
AS SELECT (ST_Dump(geom)).geom AS g FROM (SELECT ST_Boundary(ST_Union(the_geom)) AS geom FROM tiger.state) AS f;

PREPARE add_linestring AS SELECT count(*) FROM (
  SELECT topology.TopoGeo_addLinestring('topoperf', g)
  FROM topoperf.case_full_coverage_no_holes
) foo;
-- should complete in less than 15.5 seconds (winnie says she needs more than 15 seconds)
SELECT performs_ok( 'add_linestring', 15500, 'US States boundary added') ;

SELECT pass((SELECT count(1) FROM topoperf.face)::text || ' faces');

SELECT pass((SELECT count(1) FROM topoperf.edge)::text || ' edges');
SELECT * FROM finish();
ROLLBACK;
