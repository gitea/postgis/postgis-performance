\i pgtap/setup.sql
SELECT plan(4);
PREPARE geocode_1 AS SELECT ST_AsText(ST_SnapToGrid(geomout, 0.0001, 0.0001) )
FROM geocode('1808 New Hampshire Ave NW, Washington DC, DC 20009',1);

-- run 20 iterations and require each two finish between 10 and 250 ms
SELECT performs_within('geocode_1', 10, 300, 20, 'geocode 1');

PREPARE geocode_2 AS SELECT ST_AsText(ST_SnapToGrid(geomout, 0.0001, 0.0001) )
FROM geocode('1808 New Hampshire Ave NW, Washington DC, DC 20009',1);

-- run 20 iterations and require each two finish between 10 and 250 ms
SELECT performs_within('geocode_2', 10, 300, 20, 'geocode 2');

SELECT ok( (SELECT ST_AsText(ST_SnapToGrid(geomout, 0.0005, 0.0005) )
FROM geocode('1255 22nd St NW, Washington, DC 20037',1)) = 'POINT(-77.0485 38.9065)' , 'DC 2');


-- run 20 iterations and require each two finish between 10 and 250 ms
SELECT performs_within($sql$SELECT ST_AsText(ST_SnapToGrid(geomout, 0.0001, 0.0001) )
FROM geocode('1 Devonshire Place, PH 301, Boston MA 02109',1)$sql$, 10, 300, 20, 'geocode MA 1');

SELECT * FROM finish();
