\i pgtap/setup.sql
SELECT plan(2);
set work_mem='500MB';

SELECT ok( (SELECT pprint_addy(addy[1])
FROM reverse_geocode(ST_Point(-77.0489, 38.90658)))
  = '1260 22nd St NW, Washington, DC 20037' , 'Reverse Geocode 1 output');

PREPARE reverse_geocode_1 AS SELECT pprint_addy(addy[1])
  FROM reverse_geocode(ST_Point(-77.0489, 38.90658) ) ;

-- run 50 iterations and require average be between 5 and 650 ms, winnie is averaging 608 - slow
SELECT performs_within('reverse_geocode_1', 5, 650, 50, 'Reverse Geocode 1 Perf');
SELECT * FROM finish();
