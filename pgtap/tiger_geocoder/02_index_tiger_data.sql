\i pgtap/setup.sql
SELECT plan(1);
PREPARE index_tables AS SELECT tiger.install_missing_indexes();
-- should complete in less than 5 seconds
SELECT performs_ok( 'index_tables', 5000, 'indexes built') ;
-- #analyze all tables to make sure they are all up to date
analyze;
COMMIT;
SELECT * FROM finish();
