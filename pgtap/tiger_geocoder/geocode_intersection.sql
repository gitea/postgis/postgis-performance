\i pgtap/setup.sql
SELECT plan(2);
PREPARE gi_1 AS SELECT ST_AsText(ST_SnapToGrid(geomout, 0.0001, 0.0001) )
FROM geocode_intersection('New Hampshire Ave NW', 'Ward Place NW', 'DC', '20009');

-- run 20 iterations and require each to finish between 10 and 2500 ms
SELECT performs_within('gi_1', 10, 2500, 20, 'geocode intersection 1');


SELECT ok( (SELECT ST_AsText(ST_SnapToGrid(geomout, 0.0001, 0.0001) )
FROM geocode_intersection('New Hampshire Ave NW', 'Ward Place NW', 'DC', '20009') ORDER BY geomout LIMIT 1) = 'POINT(-77.047 38.9059)' , 'geocode intersection correct');

SELECT * FROM finish();
