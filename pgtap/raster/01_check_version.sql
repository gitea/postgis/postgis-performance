\i pgtap/setup.sql
SELECT plan(2);
SELECT pass(postgis_full_version());
SELECT ok(postgis_full_version() LIKE '%3.%', 'Postgis 3 series is installed');
SELECT * FROM finish();