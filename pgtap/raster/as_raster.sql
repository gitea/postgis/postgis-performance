\i pgtap/setup.sql
SELECT plan(2);
PREPARE as_raster_line AS SELECT ST_AsRaster(ST_Collect(geom), 500,500)
FROM osm_belarus.lines
WHERE ST_Intersects(geom, ST_MakeEnvelope(27.5, 53.9, 28, 55, 4326) ) ;

-- 20 iterations, should not take longer than avg 1 sec per iteration
SELECT performs_within('as_raster_line', 50, 1000, 20, 'As Raster Line');

PREPARE as_raster_multipolygon AS SELECT ST_AsRaster(ST_Collect(geom), 500,500) FROM osm_belarus.multipolygons WHERE ST_Intersects(geom, ST_MakeEnvelope(27.5, 53.9, 27.6, 55, 4326) );

-- 20 iterations, should not take longer than avg 1 sec per iteration
SELECT performs_within('as_raster_multipolygon', 50, 1000, 20, 'As Raster Polygon');

SELECT * FROM finish();