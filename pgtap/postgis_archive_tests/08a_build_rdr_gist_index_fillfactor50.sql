\i pgtap/setup.sql
SELECT plan(1);
set work_mem='500MB';

CREATE OR REPLACE FUNCTION create_rdr_index() RETURNS boolean  AS
$$
BEGIN
DROP INDEX IF EXISTS public.ix_roads_rdr_gist;
CREATE INDEX ix_roads_rdr_gist ON public.roads_rdr USING gist (geom) WITH (FILLFACTOR=50);
DROP INDEX IF EXISTS public.ix_roads_rdr_2d_gist;
CREATE INDEX ix_roads_rdr_2d_gist ON public.roads_rdr_2d USING gist(geom) WITH (FILLFACTOR=50);
DROP INDEX IF EXISTS public.ix_rdr_2d_points_gist;
CREATE INDEX ix_rdr_2d_points_gist ON public.rdr_2d_points USING gist(geom) WITH (FILLFACTOR=50);
RETURN true;

END;
$$
language plpgsql;


PREPARE rdr_index AS SELECT create_rdr_index() ;
-- allow for 60 seconds to build index
SELECT performs_ok( 'rdr_index', 60000, 'Index built: fillfactor 50 roads_rdr, roads_rdr_2d, rdr_2d_points') ;
analyze public.roads_rdr;


DROP FUNCTION create_rdr_index();

COMMIT;

