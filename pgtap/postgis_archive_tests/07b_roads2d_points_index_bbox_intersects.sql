\i pgtap/setup.sql
SELECT plan(1);

PREPARE bbox_intersect_check_roads_points_2d AS select count(*) from roads_rdr_2d a, rdr_2d_points b where a.geom && b.geom;

SELECT performs_within('bbox_intersect_check_roads_points_2d', 300, 20000, 1, 'bbox intersect roads_2d against roads_points_2d with index');

SELECT * FROM finish();
