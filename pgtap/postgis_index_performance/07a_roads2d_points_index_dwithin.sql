\i pgtap/setup.sql
SELECT plan(1);

PREPARE dwithin_check_roads_2d_points_2d AS select count(*) from roads_rdr_2d a, rdr_2d_points b WHERE ST_DWithin(a.geom, b.geom,10);


SELECT performs_within('dwithin_check_roads_2d_points_2d', 300, 50000, 1, 'dwithin roads roads_rdr_2d against rdr_2d_points with index');

SELECT * FROM finish();
