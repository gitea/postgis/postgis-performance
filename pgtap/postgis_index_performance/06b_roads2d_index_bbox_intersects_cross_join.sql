\i pgtap/setup.sql
SELECT plan(2);

PREPARE bbox_intersect_check_crossjoin_roads_2d AS select count(*) from roads_rdr_2d a, roads_rdr_2d b where a.geom && b.geom;

SELECT indexes_are(
    'public',
    'roads_rdr_2d',
    ARRAY[ 'ix_roads_rdr_2d_gist'], 'Check has public.roads_rdr_2d has gist'
);

-- just one test 300 +- 20000
SELECT performs_within('bbox_intersect_check_crossjoin_roads_2d', 300, 20000, 1, 'bbox intersect roads_2d against roads with index');

SELECT * FROM finish();
