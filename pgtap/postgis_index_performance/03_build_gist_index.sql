\i pgtap/setup.sql
SELECT plan(4);
set work_mem='500MB';

CREATE OR REPLACE FUNCTION create_osm_multipolygons_index() RETURNS boolean  AS
$$
BEGIN
DROP INDEX IF EXISTS osm_belarus.ix_osm_belarus_multipolygons_gist;
CREATE INDEX ix_osm_belarus_multipolygons_gist ON osm_belarus.multipolygons USING gist(geom);
RETURN true;
END;
$$
language plpgsql;

CREATE OR REPLACE FUNCTION create_osm_lines_index() RETURNS boolean  AS
$$
BEGIN
DROP INDEX IF EXISTS osm_belarus.ix_osm_belarus_lines_gist;
CREATE INDEX ix_osm_belarus_lines_gist ON osm_belarus.lines USING gist(geom);
RETURN true;
END;
$$
language plpgsql;

CREATE OR REPLACE FUNCTION create_osm_points_index() RETURNS boolean  AS
$$
BEGIN
DROP INDEX IF EXISTS osm_belarus.ix_osm_belarus_points_gist;
CREATE INDEX ix_osm_belarus_points_gist ON osm_belarus.points USING gist(geom);
RETURN true;
END;
$$
language plpgsql;

CREATE OR REPLACE FUNCTION create_rdr_index() RETURNS boolean  AS
$$
BEGIN
DROP INDEX IF EXISTS public.ix_roads_rdr_gist;
CREATE INDEX ix_roads_rdr_gist ON public.roads_rdr USING gist (geom) WITH (FILLFACTOR=50);
DROP INDEX IF EXISTS public.ix_roads_rdr_2d_gist;
CREATE INDEX ix_roads_rdr_2d_gist ON public.roads_rdr_2d USING gist(geom);
DROP INDEX IF EXISTS public.ix_rdr_2d_points_gist;
CREATE INDEX ix_rdr_2d_points_gist ON public.rdr_2d_points USING gist(geom);
RETURN true;

END;
$$
language plpgsql;



PREPARE osm_index_multipolygons AS SELECT create_osm_multipolygons_index();
-- allow for 60 seconds to build index
SELECT performs_ok( 'osm_index_multipolygons', 60000, 'Index built: multipolygons') ;
analyze osm_belarus.multipolygons;

PREPARE osm_index_lines AS SELECT create_osm_lines_index();

-- allow for 60 seconds to build index
SELECT performs_ok( 'osm_index_lines', 60000, 'Index built: lines') ;
analyze osm_belarus.lines;

PREPARE osm_index_points AS SELECT create_osm_points_index();
-- allow for 60 seconds to build index
SELECT performs_ok( 'osm_index_points', 60000, 'Index built: points') ;
analyze osm_belarus.points;

PREPARE rdr_index AS SELECT create_rdr_index() ;
-- allow for 60 seconds to build index
SELECT performs_ok( 'rdr_index', 60000, 'Index built: roads_rdr, roads_rdr_2d, rdr_2d_points') ;
analyze public.roads_rdr;


DROP FUNCTION create_osm_multipolygons_index();
DROP FUNCTION create_osm_lines_index();
DROP FUNCTION create_osm_points_index();
DROP FUNCTION create_rdr_index();

COMMIT;

