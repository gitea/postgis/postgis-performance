\i pgtap/setup.sql
SELECT plan(9);

PREPARE bbox_intersect_multipolygon_no_match AS SELECT * FROM osm_belarus.multipolygons WHERE geom && ST_SetSRID(ST_Point(10,10),4326);

PREPARE bbox_intersect_multipolygon_match AS SELECT * FROM osm_belarus.multipolygons WHERE geom && ST_MakeEnvelope(27.5, 53.9, 27.6, 55, 4326);

PREPARE bbox_intersect_line_no_match AS SELECT * FROM osm_belarus.lines WHERE geom && ST_SetSRID(ST_Point(10,10),4326);

PREPARE bbox_intersect_line_match AS SELECT * FROM osm_belarus.lines WHERE geom && ST_MakeEnvelope(27.5, 53.9, 27.6, 55, 4326) ;

PREPARE bbox_intersect_point_no_match AS SELECT * FROM osm_belarus.points WHERE geom && ST_SetSRID(ST_Point(10,10),4326) ;

PREPARE bbox_intersect_point_match AS SELECT * FROM osm_belarus.points WHERE geom && ST_MakeEnvelope(27.5, 53.9, 27.6, 55, 4326);

SELECT indexes_are(
    'osm_belarus',
    'multipolygons',
    ARRAY[ 'ix_osm_belarus_multipolygons_gist'], 'Check has osm_belarus.multipolygons has gist'
);

SELECT indexes_are(
    'osm_belarus',
    'lines',
    ARRAY[ 'ix_osm_belarus_lines_gist'], 'Check has osm_belarus.lines geom gist'
);

SELECT indexes_are(
    'osm_belarus',
    'points',
    ARRAY[ 'ix_osm_belarus_points_gist'], 'Check has osm_belarus.points geom gist'
);

SELECT performs_within('bbox_intersect_multipolygon_no_match', 0, 600, 50, 'bbox intersect multipolygon check no match with index');

SELECT performs_within('bbox_intersect_multipolygon_match', 0, 600, 100, 'bbox intersect multipolygon check match with index');

SELECT performs_within('bbox_intersect_line_no_match', 0, 600, 50, 'bbox intersect linestring check no match with index');

SELECT performs_within('bbox_intersect_line_match', 0, 600, 100, 'bbox intersect linestring check match with index');

SELECT performs_within('bbox_intersect_point_no_match', 0, 600, 100, 'bbox intersect pointcheck no match with index');

SELECT performs_within('bbox_intersect_point_match', 0, 600, 100, 'bbox intersect point check match with index');


SELECT * FROM finish();
