\i pgtap/setup.sql
SELECT plan(9);

PREPARE intersect_check_multipolygon_no_match AS SELECT * FROM osm_belarus.multipolygons WHERE ST_Intersects(geom, ST_SetSRID(ST_Point(10,10),4326) );

PREPARE intersect_check_multipolygon_match AS SELECT * FROM osm_belarus.multipolygons WHERE ST_Intersects(geom, ST_MakeEnvelope(27.5, 53.9, 27.6, 55, 4326) );

PREPARE intersect_check_line_no_match AS SELECT * FROM osm_belarus.lines WHERE ST_Intersects(geom, ST_SetSRID(ST_Point(10,10),4326) );

PREPARE intersect_check_line_match AS SELECT * FROM osm_belarus.lines WHERE ST_Intersects(geom, ST_MakeEnvelope(27.5, 53.9, 27.6, 55, 4326) );

PREPARE intersect_check_point_no_match AS SELECT * FROM osm_belarus.points WHERE ST_Intersects(geom, ST_SetSRID(ST_Point(10,10),4326) );

PREPARE intersect_check_point_match AS SELECT * FROM osm_belarus.points WHERE ST_Intersects(geom, ST_MakeEnvelope(27.5, 53.9, 27.6, 55, 4326) );

SELECT indexes_are(
    'osm_belarus',
    'multipolygons',
    ARRAY[ 'ix_osm_belarus_multipolygons_gist'], 'Check has osm_belarus.multipolygons has gist'
);

SELECT indexes_are(
    'osm_belarus',
    'lines',
    ARRAY[ 'ix_osm_belarus_lines_gist'], 'Check has osm_belarus.lines geom gist'
);

SELECT indexes_are(
    'osm_belarus',
    'points',
    ARRAY[ 'ix_osm_belarus_points_gist'], 'Check has osm_belarus.points geom gist'
);

SELECT performs_within('intersect_check_multipolygon_no_match', 0, 600, 50, 'intersect multipolygon check no match with index');

SELECT performs_within('intersect_check_multipolygon_match', 0, 600, 100, 'intersect multipolygon check match with index');

SELECT performs_within('intersect_check_line_no_match', 0, 600, 50, 'intersect linestring check no match with index');

SELECT performs_within('intersect_check_line_match', 0, 600, 100, 'intersect linestring check match with index');

SELECT performs_within('intersect_check_point_no_match', 0, 600, 100, 'intersect pointcheck no match with index');

SELECT performs_within('intersect_check_point_match', 0, 600, 100, 'intersect point check match with index');


SELECT * FROM finish();
