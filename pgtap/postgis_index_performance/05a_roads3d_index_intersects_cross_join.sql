\i pgtap/setup.sql
SELECT plan(2);

PREPARE intersect_check_crossjoin_roads AS select count(*) from roads_rdr a, roads_rdr b where ST_Intersects(a.geom, b.geom);

SELECT indexes_are(
    'public',
    'roads_rdr',
    ARRAY[ 'ix_roads_rdr_gist'], 'Check has public.roads_rdr has gist'
);

SELECT performs_within('intersect_check_crossjoin_roads', 300, 20000, 1, 'intersect roads against roads with index');

SELECT * FROM finish();
