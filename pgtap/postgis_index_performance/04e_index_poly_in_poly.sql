\i pgtap/setup.sql
SELECT plan(3);

SELECT indexes_are(
    'osm_belarus',
    'multipolygons',
    ARRAY[ 'ix_osm_belarus_multipolygons_gist'], 'Check has osm_belarus.multipolygons has gist'
);

PREPARE intersect_poly_in_poly AS SELECT poly.osm_id, count(*)
FROM osm_belarus.multipolygons AS poly INNER JOIN osm_belarus.multipolygons AS poly2 ON ST_Intersects(poly.geom,poly2.geom )
WHERE poly.osm_id IN('20699','20700', '70719', '70721')
GROUP BY poly.osm_id;

PREPARE bbox_line_in_poly AS SELECT poly.osm_id, count(*)
FROM osm_belarus.multipolygons AS poly INNER JOIN osm_belarus.multipolygons AS poly2 ON poly.geom && poly2.geom
WHERE poly.osm_id IN('20699','20700', '70719', '70721')
GROUP BY poly.osm_id;


SELECT performs_within('intersect_poly_in_poly', 0, 1300, 50, 'intersect poly in poly');

SELECT performs_within('bbox_line_in_poly', 0, 1300, 50, 'bbox intersect poly in poly');

SELECT * FROM finish();
