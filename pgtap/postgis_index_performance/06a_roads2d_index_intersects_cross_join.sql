\i pgtap/setup.sql
SELECT plan(2);

PREPARE intersect_check_crossjoin_roads_2d AS select count(*) from roads_rdr_2d a, roads_rdr_2d b where ST_Intersects(a.geom, b.geom);

SELECT indexes_are(
    'public',
    'roads_rdr_2d',
    ARRAY[ 'ix_roads_rdr_2d_gist'], 'Check has public.roads_rdr_2d has gist'
);

SELECT performs_within('intersect_check_crossjoin_roads_2d', 300, 20000, 1, 'intersect roads roads_rdr_2d against roads_rdr_2d with index');

SELECT * FROM finish();
