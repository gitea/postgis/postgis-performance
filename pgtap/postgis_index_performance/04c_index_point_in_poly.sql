\i pgtap/setup.sql
SELECT plan(4);

SELECT indexes_are(
    'osm_belarus',
    'multipolygons',
    ARRAY[ 'ix_osm_belarus_multipolygons_gist'], 'Check has osm_belarus.multipolygons has gist'
);

SELECT indexes_are(
    'osm_belarus',
    'points',
    ARRAY[ 'ix_osm_belarus_points_gist'], 'Check has osm_belarus.points geom gist'
);

PREPARE intersect_point_in_poly AS SELECT poly.osm_id, count(*)
FROM osm_belarus.multipolygons AS poly INNER JOIN osm_belarus.points AS poi ON ST_Intersects(poly.geom,poi.geom )
WHERE poly.osm_id IN('20699','20700', '70719', '70721')
GROUP BY poly.osm_id;

PREPARE bbox_point_in_poly AS SELECT poly.osm_id, count(*)
FROM osm_belarus.multipolygons AS poly INNER JOIN osm_belarus.points AS poi ON poly.geom && poi.geom
WHERE poly.osm_id IN('20699','20700', '70719', '70721')
GROUP BY poly.osm_id;


SELECT performs_within('intersect_point_in_poly', 0, 1300, 50, 'intersect point in poly');

SELECT performs_within('bbox_point_in_poly', 0, 1300, 50, 'bbox intersect point in poly');

SELECT * FROM finish();
