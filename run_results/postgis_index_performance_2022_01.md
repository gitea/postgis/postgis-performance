Columns are in milliseconds except where stated otherwise.
These tests were running against PG15 head  [patch](https://www.postgresql.org/message-id/attachment/129685/02_reduce_page_overlap_of_gist_indexes_built_using_sorted_method.patch) applied

|System    |Index build|Intersect 1|bbox 1|Intersect / bbox (point in poly)|Intersect / bbox (line in poly)|intersect poly in poly|roads_rdr intersects cross join roads_rdr|roads3d index box cross join|roads 2d index intersects cross join|roads 2d index box cross join|dwithin roads roads_rdr_2d against rdr_2d_points|Total wall clock secs|
|:---|---:|---:|---:|---:|---:|---:|---:|---:|---:|---:|---:|---:|
|*Windows 10 (mingw-64)*::| PostgreSQL 15devel POSTGIS="3.3.0dev 3.2.0-155-g56db21dab" GEOS="3.10.0-CAPI-1.16.0"|||||
|pg 15 with gist_sort|19727|13370|4078|27904|27841|30128|2775|1502|2667|1344|1644|133|
|pg 15 without gist sort|33777|13544|4248|27912|28125|30411|2451|1073|2712|1397|1691|148|
||||||||||||||
||||||||||||||
|Reallie (Debian 11)| PostgreSQL 15devel POSTGIS="3.3.0dev 3.2.0-263-gbf01900ee" [EXTENSION] PGSQL="150" GEOS="3.9.0-CAPI-1.16.2"
|pg 15 with gist_sort|45817|22976|10301|100714|99709|105400|6401|2927|6265|2751|3584|407|
|pg 15 without gist sort|75583|23890|11055|100884|99635|106279|5500|1997|6099|2733|3499|437|

The tests are in pgtap/postgis_index_performance

|Test|File|
|:---|:---|
Index build|03_build_gist_index.sql|
Intersect 1|04a_index_intersect.sql|
bbox 1|04b_index_bbox_intersect.sql|
Intersect / bbox (point in poly)|04c_index_point_in_poly.sql|
Intersect / bbox (line in poly)|04d_index_line_in_poly.sql|
intersect poly in poly|04e_index_poly_in_poly.sql|
roads_rdr (3d)  box cross join roads_rdr| 05a_roads3d_index_intersects_cross_join.sql|
roads3d index intersects cross join|05b_roads3d_index_bbox_intersects_cross_join.sql|
roads 2d index intersects cross join|06a_roads2d_index_intersects_cross_join.sql|
roads 2d index box cross join|06b_roads2d_index_bbox_intersects_cross_join.sql|
